# Retrieve the path to Makefile parent folder
.MAKEFILE := $(lastword $(MAKEFILE_LIST))
.MAKEFILE_PATH := $(shell readlink $(.MAKEFILE) || realpath $(.MAKEFILE))
.MAKEFILE_PARENT_PATH := $(realpath $(dir $(.MAKEFILE_PATH)))

# Path to `yocto-env` script
YOCTO_TOOL_PATH ?= $(.MAKEFILE_PARENT_PATH)
YOCTO_ENV ?= $(YOCTO_TOOL_PATH)/yocto-env

# We assume the workspace path is the parent directory of the Makefile
WORKSPACE_PATH := $(realpath $(dir $(abspath $(lastword $(MAKEFILE_LIST)))))
WORKING_DIR_PATH := $(realpath $(PWD))

# repo manifest config
MANIFEST_REPO ?= git@gitlab.com:acrucifix-tns/yocto-demo.git
MANIFEST_BRANCH ?= main
MANIFEST_FILE ?= default.xml
MANIFEST_PATH := $(WORKSPACE_PATH)/.repo/manifests/$(MANIFEST_FILE)

RUN_CMD := $(YOCTO_ENV) --workdir $(WORKSPACE_PATH) --cd $(WORKING_DIR_PATH)
BUILD_DIR_PATH ?= $(WORKSPACE_PATH)/build
TOOLS_PATH := $(YOCTO_TOOL_PATH)/tools
TOOLS_DOCKER_PATH := /opt/tools
LAYERS_PATH := $(TOOLS_DOCKER_PATH)/layers.py
MANIFEST_TOOL_PATH := $(TOOLS_DOCKER_PATH)/manifest.py
ELF_WELCOME_PATH := $(TOOLS_DOCKER_PATH)/elf-welcome.sh
TOOLS_ARGS := --volume "$(TOOLS_PATH):$(TOOLS_DOCKER_PATH):ro"

SCRIPT_PATH := $(shell mktemp -t -u cmd_XXXXXX.sh)
SCRIPT_DOCKER_PATH := /tmp/cmd.sh
ENV_SETUP_DOCKER_PATH := /tmp/env-setup
SCRIPT_ARGS := --volume "$(SCRIPT_PATH):$(SCRIPT_DOCKER_PATH):ro" --env ENTRYPOINT=$(SCRIPT_DOCKER_PATH)

run_in_docker = $(RUN_CMD) -c bash -c "$(1)"
start_script = echo "\#!/bin/bash -e" > $(SCRIPT_PATH) && chmod +x $(SCRIPT_PATH)
append_script = echo $(1) >> $(SCRIPT_PATH)
remove_script = rm -f $(SCRIPT_PATH)


repo-init:
	@echo "# repo-init"
	@$(call run_in_docker,'repo init --manifest-url="$(MANIFEST_REPO)" --manifest-name="$(MANIFEST_FILE)" --manifest-branch="$(MANIFEST_BRANCH)"')

	@find $(WORKSPACE_PATH) -maxdepth 1 -type l -exec bash -c '[[ $$(realpath "$$(readlink {})") = $$(realpath ".repo/manifests")/*.xml ]] && rm -f {}' \;
	@ln -s -f ".repo/manifests/$(MANIFEST_FILE)" $(WORKSPACE_PATH)/manifest.xml


$(WORKSPACE_PATH)/.repo: repo-init


repo-sync: $(WORKSPACE_PATH)/.repo/
	@echo "# repo-sync"
	@$(call run_in_docker,'repo sync -j8')


run:
	@$(call start_script)

# retrieve config name
	@$(eval YOCTO_CONFIG := $(word 2, $(MAKECMDGOALS)))
	@$(eval BUILD_DIR_PATH = $(WORKSPACE_PATH)/build/$(YOCTO_CONFIG))
# find oe-init-build-env path
	@$(eval OE_INIT_BUILD_ENV_PATH := $(shell find "$(WORKSPACE_PATH)" -path */*/poky/oe-init-build-env))
# set envionment variables
	@$(call append_script,"export YOCTO_CONFIG=$(YOCTO_CONFIG)")
	@$(call append_script,"export YOCTO_MANIFEST=$(MANIFEST_PATH)")
	@$(call append_script,"export YOCTO_WORKSPACE=$(WORKSPACE_PATH)")
	@$(call append_script,"TEMPLATE_CONF_PATH=\$$($(MANIFEST_TOOL_PATH) --manifest=$(MANIFEST_PATH) config[@name=\'$(YOCTO_CONFIG)\']/template-conf --tag path --basic-output)")
	@$(call append_script,"")
	@$(call append_script,"if [[ -n \$$TEMPLATE_CONF_PATH ]]; then")
	@$(call append_script,"    export TEMPLATECONF=\$$(PWD=\"$(WORKSPACE_PATH)\" realpath \"\$$TEMPLATE_CONF_PATH\")")
	@$(call append_script,"fi")
	@$(call append_script,"")
# setup build environment
	@$(call append_script,"mkdir -p $(BUILD_DIR_PATH)")
	@$(call append_script,"source $(OE_INIT_BUILD_ENV_PATH) $(BUILD_DIR_PATH)")
# add layers
	@$(call append_script,"$(MANIFEST_TOOL_PATH) --manifest=$(MANIFEST_PATH) config[@name=\'$(YOCTO_CONFIG)\']/layer layer --tag path --basic-output | awk '{print \"$(WORKSPACE_PATH)/\" \$$0}' | xargs bitbake-layers add-layer")
	@$(call append_script,"")
# add the environment variables
	@$(call append_script,"$(MANIFEST_TOOL_PATH) --manifest=$(MANIFEST_PATH) config[@name=\'$(YOCTO_CONFIG)\']/env-var env-var --basic-output | sed 's/^env-var./export /' > $(ENV_SETUP_DOCKER_PATH)")
	@$(call append_script,"source $(ENV_SETUP_DOCKER_PATH)")

# add project.conf
	@$(eval CONF_PATH := $(BUILD_DIR_PATH)/conf)
	@$(eval PROJECT_CONF_PATH := $(CONF_PATH)/project.conf)
	@$(eval LOCAL_CONF_PATH := $(CONF_PATH)/local.conf)
	@$(eval INCLUDE_PROJECT_CONF := include $(shell basename "$(PROJECT_CONF_PATH)"))
	@$(call append_script,"$(MANIFEST_TOOL_PATH) --manifest=$(MANIFEST_PATH) config[@name=\'$(YOCTO_CONFIG)\']/project-var project-var --basic-output | sed 's/^project-var.//' > $(PROJECT_CONF_PATH)")
	@$(call append_script,"grep '$(INCLUDE_PROJECT_CONF)' $(LOCAL_CONF_PATH) || echo '$(INCLUDE_PROJECT_CONF)' >> $(LOCAL_CONF_PATH)")

# print welcome
	@$(call append_script,"$(ELF_WELCOME_PATH)")
	@$(call append_script,"")

# execute command
	@$(call append_script,"exec "$$\@"")

	@$(RUN_CMD) $(TOOLS_ARGS) $(SCRIPT_ARGS)
	@$(call remove_script)


manifest: repo-init repo-sync


all: manifest run


clean:
	@$(eval YOCTO_CONFIG := $(word 2, $(MAKECMDGOALS)))
	@echo "# clean"
	@$(call run_in_docker,'rm -rf $(BUILD_DIR_PATH)/$(YOCTO_CONFIG)')


.DEFAULT_GOAL := all
.PHONY: all clean manifest run repo-init repo-sync

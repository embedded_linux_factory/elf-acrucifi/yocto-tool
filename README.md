# yocto-tool

## Description

This tool is a prototype that shall be used in association with
[build-environment](https://gitlab.com/acrucifix-tns/build-environment) project.

`build-environment` project provides a `yocto-env` docker image
and launching script that is used by `yocto-tool`.

This tool allow to start a docker container providing all tools required to develop
and build a Yocto project based on a git-repo manifest file.

## Setup

### Build your yocto-env docker image

Retrieve the [build-environment](https://gitlab.com/acrucifix-tns/build-environment)
project. Then build the `yocto-env` docker image.

```bash
$ cd ~/workspace/
$ git clone https://gitlab.com/acrucifix-tns/build-environment.git
Cloning into 'build-environment'...
remote: Enumerating objects: 92, done.
remote: Counting objects: 100% (92/92), done.
remote: Compressing objects: 100% (87/87), done.
remote: Total 92 (delta 41), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (92/92), 25.94 KiB | 12.97 MiB/s, done.
Resolving deltas: 100% (41/41), done.
$ cd build-environment/
$ make yocto-env
docker build ~/workspace/build-environment/build-environment --tag build-environment
[+] Building 124.1s (11/11) FINISHED
docker build ~/workspace/build-environment/yocto-env --build-arg FROM=build-environment:latest --tag yocto-env
[+] Building 101.8s (10/10) FINISHED
```

### Link yocto-env command

```bash
ln -s ~/workspace/build-environment/yocto-env/yocto-env ~/workspace/yocto-tool/yocto-env
```

## Usage

### Manifest Example

The manifest file used for the Yocto project extends the
[git-repo manifest](https://github.com/GerritCodeReview/git-repo/blob/main/docs/manifest-format.md)
format. This allow to use [git-repo](https://github.com/GerritCodeReview/git-repo)
tool as versioning tool.

```xml
<?xml version="1.0" encoding="UTF-8"?>
<manifest>
  <default sync-j="4" revision="kirkstone"/>

  <!-- Yocto -->
  <remote fetch="https://git.yoctoproject.org/git" name="yocto"/>
  <project remote="yocto" name="poky" path="layers/poky"/>

  <!-- raspberry Pi -->
  <remote fetch="https://github.com/agherzan" name="raspberrypi"/>
  <project remote="raspberrypi" name="meta-raspberrypi" path="layers/meta-raspberrypi"/>

  <!-- openembedded -->
  <remote fetch="https://github.com/openembedded" name="oe"/>
  <project remote="oe" name="meta-openembedded" path="layers/meta-openembedded"/>
  <layer path="layers/meta-openembedded/meta-oe"/>
  <layer path="layers/meta-openembedded/meta-python"/>

  <!-- Config -->
  <remote fetch="https://my.git-server.com" name="my-server"/>
  <project remote="my-server" name="yocto-config" path="config" revision="master"/>

  <!-- Config definition for raspberrypi3 -->
  <config name="raspberrypi3">
    <!-- Add a specific layer for this configuration -->
    <layer path="layers/meta-raspberrypi"/>

    <!--
      Use an alternative template for bitbake configuration files
      the path will be as for TEMPLATECONF to init the build environment
    -->
    <template-conf path="config/rpi3"/>

    <!--
      Set environment variable to <build_dir>/conf/project.conf
      project.conf will be included to <build_dir>/conf/local.conf
    -->
    <project-var ENABLE_UART="1"/>
  </config>

  <!-- Config definition for qemux86-64 -->
  <config name="qemux86-64">
    <!-- Set an environment variable inside docker container -->
    <env-var SDKMACHINE="i686"/>
  </config>

</manifest>
```

### Setup a project

* Create a git repository hosting the manifest file.
* Add a new manifest file named `default.xml` based on the above example.
* Create a directory where will be located your project.
* Create a symlink to the Makefile inside your project.
* Your are ready to use start your yocto project.

```bash
mkdir -p ~/workspace/my-project
cd ~/workspace/my-project
ln -s /path/to/yocto-tool/Makefile
```

### Using yocto-tool

#### repo init

```bash
$ make repo-init MANIFEST_REPO=https://my.git-server.com/my-project.git MANIFEST_BRANCH=master
# repo-init

repo has been initialized in ~/workspace/my-project
```

#### repo sync

```bash
$ make repo-sync
# repo-sync
Fetching: 100% (7/7), done in 105.039s
repo sync has finished successfully.
```

#### enter to build environment

```bash
$ make run raspberrypi3
You had no conf/local.conf file. This configuration file has therefore been
created for you from ~/workspace/yocto_demo/conf/rpi3/local.conf.sample
You may wish to edit it to, for example, select a different MACHINE (target
hardware). See conf/local.conf for more information as common configuration
options are commented.

You had no conf/bblayers.conf file. This configuration file has therefore been
created for you from ~//workspace/yocto_demo/conf/rpi3/bblayers.conf.sample
To add additional metadata layers into your configuration please add entries
to conf/bblayers.conf.

The Yocto Project has extensive documentation about OE including a reference
manual which can be found at:
    https://docs.yoctoproject.org

For more information about OpenEmbedded see the website:
    https://www.openembedded.org/

NOTE: Starting bitbake server...

   ,(%%%%%%%P°  %%%%%%%%%%%%%%%%%%%%%%),
  (%%%%%%°      %%%%%%%%%%%%%%%%%%%%%%%%)
  (%%%%%%       %%%%%%%%%%%%%%%%%%%%j%%%)
                                     "²0)
                                      ,%)        .----------------.  .----------------.  .----------------.
  (%%%%%%%%%%%%%%%%%%%%%%%%%%%%bo,_  ,%%)       | .--------------. || .--------------. || .--------------. |
  (%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%,%%%)       | |  _________   | || |   _____      | || |  _________   | |
  (%%%%%%            ,%%%%%%%%%%%%%%%%%%)       | | |_   ___  |  | || |  |_   _|     | || | |_   ___  |  | |
  (%%%%%%            %%%%%%%%%%%%%%%%%%%)       | |   | |_  \_|  | || |    | |       | || |   | |_  \_|  | |
  (%%%%%%            °%%%%%%%%%%%%%%%%%%)       | |   |  _|  _   | || |    | |   _   | || |   |  _|      | |
  (%%%%%%       y     °%%%%%%%%%%%%%%%%%)       | |  _| |___/ |  | || |   _| |__/ |  | || |  _| |_       | |
  (%%%%%%       %b,      °%%%%%%%%%%%%%%)       | | |_________|  | || |  |________|  | || | |_____|      | |
  (%%%%%%       %%%%b,           °%%%%%%)       | |              | || |              | || |              | |
  (%%%%%%       %%%%%%%%%%b,         °%%)       | '--------------' || '--------------' || '--------------' |
  (%%%%%%       %%%%%%%%%%%%%%%c,      °)        '----------------'  '----------------'  '----------------'
  (%%%%%%       %%%%%%%%%%%%%%%%%,      °
  (%%%%%%b      %%%%%%%%%%%%%%%%%%      ,                       EMBEDDED . LINUX . FACTORY
  (%%%%%%%b     °%%%%%%%%%%%%%%%%°     ,e
  (%%%%%%%%b,      °%%%%%%%%%%°       ,%)
   °(%%%%%%%%%%b,                  ,%%)°




**********************************************************************************************************
*                                      Yocto environment is completed                                    *
**********************************************************************************************************

You can now run commands and build any image.
For example you can execute following command:

bitbake core-image-minimal

user@yocto-env:~/workspace/yocto_demo/build/raspberrypi3$
```

#### Have fun

You shall now be able to build your Yocto project.

## How-to contribute

To contribute to this project, please ensure that the following tools are installed:

* gitlint
* pre-commit
* shfmt
* shellcheck

You can automatically check and install the tools using the following command:

```shell
$ ./dev-env-setup.sh
Start of dev environment setup
Checking tools...
* pip... OK
* shfmt... OK
* shellcheck... Installed
* pre-commit... OK
* gitlint... OK

Checking hooks...
* pre-commit... Installed
* commit-msg... OK

Dev environment is ready!
```

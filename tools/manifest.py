#!/usr/bin/env python3

import os
from argparse import ArgumentParser, ArgumentTypeError
from pathlib import Path
from typing import Dict, Generator, Iterable, TextIO, Tuple, Union

import subprocess
from lxml import etree


def get_path(path: Path, working_dir: Path, absolute: bool) -> Path:
    abs_path = Path(path).absolute()

    if absolute:
        return abs_path
    else:
        return os.path.relpath(abs_path, working_dir.absolute())


def iter_xml(
    file: Union[TextIO, Path], filters: Union[str, Iterable[str]] = "*"
) -> Generator[Tuple[str, Dict[str, str]], None, None]:
    root = etree.parse(file).getroot()
    if root.tag != "manifest":
        return

    if isinstance(filters, str):
        filters = [filters]

    try:
        for xpath_filter in filters:
            root.xpath(xpath_filter)
    except etree.XPathEvalError as exc:
        raise ValueError(
            f"Invalid XPath expression: '{xpath_filter}'"
        ) from exc

    for xpath_filter in filters:
        for child in root.findall(xpath_filter):
            yield child.tag, child.attrib


def repo_top_level() -> Path:
    result = subprocess.run(
        ["repo", "--show-toplevel"],
        stdout=subprocess.PIPE,
        stderr=subprocess.DEVNULL,
    )
    result.check_returncode()

    return Path(result.stdout.decode().strip())


def repo_manifest() -> Path:
    repo_path = repo_top_level() / ".repo"
    manifests_path = repo_path / "manifests"
    manifest_xml = repo_path / "manifest.xml"

    with manifest_xml.open("r", encoding="utf-8") as f:
        _, include_entry = next(iter_xml(f, "./include"))

        return manifests_path.joinpath(include_entry["name"])


def existing_file_path(arg: str) -> Path:
    try:
        file_path = Path(arg)
        assert file_path.exists()
        assert file_path.is_file()
        return file_path
    except Exception as exc:
        raise ArgumentTypeError(
            f"'{arg}' shall match to an existing file path."
        ) from exc


def main():
    parser = ArgumentParser(
        description="List tag attributes from a manifest file"
    )

    parser.add_argument(
        "--manifest",
        help="Manifest file path, default: "
        f"{os.path.relpath(repo_manifest(), Path.cwd())}",
        default=repo_manifest(),
        type=existing_file_path,
    )

    parser.add_argument(
        "filters",
        nargs="*",
        type=str,
        help="XPath filters, "
        "see https://www.w3schools.com/xml/xpath_syntax.asp",
    )

    parser.add_argument("--tag", "-t", type=str, help="Select XML tag")

    parser.add_argument(
        "--basic-output",
        "-b",
        action="store_true",
        help="Basic output print, easy to use in bash scripts",
    )

    namespace = parser.parse_args()

    try:
        for tag, attrib in iter_xml(namespace.manifest, namespace.filters):
            if namespace.tag:
                if namespace.tag in attrib:
                    if namespace.basic_output:
                        print(f"{attrib[namespace.tag]}")
                    else:
                        print(f"{tag}: '{attrib[namespace.tag]}'")
                else:
                    continue
            else:
                if namespace.basic_output:
                    for k, v in attrib.items():
                        print(f"{tag}.{k}='{v}'")
                else:
                    value = " ".join(f"{k}='{v}'" for k, v in attrib.items())
                    print(f"{tag}: {value}")

    except etree.XMLSyntaxError as exc:
        parser.error(exc)
    except ValueError as exc:
        parser.error(exc)


if __name__ == "__main__":
    main()

#!/bin/bash

set -e

SCRIPT_DIR=$(dirname "${BASH_SOURCE[0]}")
ESC="\033"
ANSI_RED="${ESC}[31m"
ANSI_GREEN="${ESC}[32m"
ANSI_YELLOW="${ESC}[33m"
ANSI_RESET="${ESC}[0m"

echo "Start of dev environment setup"

TOOLS=(
	#TOOL_NAME,CHECK_CMD,INSTALL_CMD
	"pip,which pip,sudo apt install python3-pip"
	"shfmt,which shfmt,curl -sS https://webinstall.dev/shfmt | bash"
	"shellcheck,which shellcheck,sudo apt install shellcheck"
	"pre-commit,pip show pre-commit,pip install pre-commit"
	"gitlint,pip show gitlint,pip install gitlint"
)

echo "Checking tools..."
for tool in "${TOOLS[@]}"; do
	TOOL_NAME=${tool%%,*}
	tmp=${tool#*,}
	CHECK_CMD=${tmp%%,*}
	INSTALL_CMD=${tmp#*,}

	printf "* %s... " "$TOOL_NAME"

	if eval "$CHECK_CMD" &>/dev/null; then
		echo -e "${ANSI_GREEN}OK$ANSI_RESET"
	else
		if eval "$INSTALL_CMD" &>/dev/null; then
			echo -e "${ANSI_YELLOW}Installed$ANSI_RESET"
		else
			echo -e "${ANSI_RED}Failed$ANSI_RESET"
			echo "  Try to execute '$INSTALL_CMD'"
			exit 1
		fi
	fi
done

pushd "$SCRIPT_DIR" >/dev/null

HOOKS=(
	#HOOK_FILE,CHECK_PATTERN,INSTALL_CMD
	".git/hooks/pre-commit,pre-commit.com,pre-commit install --install-hooks"
	".git/hooks/commit-msg,gitlint,rm -f .git/hooks/commit-msg && gitlint install-hook"
)

echo
echo "Checking hooks..."
for hook in "${HOOKS[@]}"; do
	HOOK_FILE=${hook%%,*}
	tmp=${hook#*,}
	CHECK_PATTERN=${tmp%%,*}
	INSTALL_CMD=${tmp#*,}

	printf "* %s... " "$HOOK_FILE"

	if grep "$CHECK_PATTERN" "$HOOK_FILE" &>/dev/null; then
		echo -e "${ANSI_GREEN}OK$ANSI_RESET"
	else
		if eval "$INSTALL_CMD" &>/dev/null; then
			echo -e "${ANSI_YELLOW}Installed$ANSI_RESET"
		else
			echo -e "${ANSI_RED}Failed$ANSI_RESET"
			echo "  Try to execute '$INSTALL_CMD'"
			exit 1
		fi
	fi
done

popd >/dev/null

echo
echo "Dev environment is ready!"
